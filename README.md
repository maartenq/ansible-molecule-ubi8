# Ansible Molecule UBI8

[![Docker Repository on Quay](https://quay.io/repository/maartenq/ansible-molecule-ubi8/status "Docker Repository on Quay")](https://quay.io/repository/maartenq/ansible-molecule-ubi8)

Dockerfile for building container image for use for Ansible Molecule testing on UBI8. Supplies systemd init and *ansible* sudo user.
