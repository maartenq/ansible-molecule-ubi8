FROM registry.access.redhat.com/ubi8/ubi-init
LABEL maintainer="Maarten"
ENV container=docker

# Silence subscription messages.
RUN echo "enabled=0" >> /etc/dnf/plugins/subscription-manager.conf

RUN dnf makecache --timer \
    && dnf -y update \
    && dnf -y install --setopt=tsflags=nodocs \
        hostname \
        iproute \
        policycoreutils-python-utils \
        python3 \
        python3-devel \
        python3-libselinux \
        sudo \
        which \
    && dnf clean all --enablerepo='*' \
    && rm -Rf /usr/share/doc \
    && rm -Rf /usr/share/man

RUN cd /lib/systemd/system/sysinit.target.wants/; \
    for i in *; do [ $i = systemd-tmpfiles-setup.service ] || rm -f $i; done

RUN rm -f /lib/systemd/system/multi-user.target.wants/* \
    /etc/systemd/system/*.wants/* \
    /lib/systemd/system/local-fs.target.wants/* \
    /lib/systemd/system/sockets.target.wants/*udev* \
    /lib/systemd/system/sockets.target.wants/*initctl* \
    /lib/systemd/system/basic.target.wants/* \
    /lib/systemd/system/anaconda.target.wants/*

RUN sed -i -e 's/^\(Defaults\s*requiretty\)/#--- \1/'  /etc/sudoers

ENV ANSIBLE_USER=ansible SUDO_GROUP=wheel
RUN set -xe \
  && groupadd -r ${ANSIBLE_USER} \
  && useradd -m -g ${ANSIBLE_USER} ${ANSIBLE_USER} \
  && usermod -aG ${SUDO_GROUP} ${ANSIBLE_USER} \
  && sed -i "/^%${SUDO_GROUP}/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers

VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]
CMD ["/usr/sbin/init"]
